
$(document).ready(function() {

	//mylog.log("elt", elt);
	//mylog.log("elt", type,idElt);
	//mylog.log("links", links);

	setTitle("La Raffinerie");
	toggleProjects();
	toggleAllProjects();
	toggleNavMobile();
	customTabs();
	changeMobileBtnMenu();
	selectProject();
	selectProjectChildren();
	selectProjectStart()
	toggleDescription();
	initDescs();
	//isContributor();
	//loadMyStream(type, idElt);

	calendar.init("#profil-content-calendar");
	//loadCalendar(events);

	$('#follows').click(function(){
		var id = $(this).data("id");
		var isco = $(this).data("isco");
		if(isco == false){
			links.connectAjax('projects',id,userId,'citoyens','contributors', null, function(){
				$('#follows').html("Désinscrire");
				$('#follows').data("isco", true);
			});
		}else{
			links.disconnectAjax('projects',id,userId,'citoyens','contributors', null, function(){
				$('#follows').html("S'inscrire");
				$('#follows').data("isco", false);
			});
		}
		
	});

});

function loadCalendar(typeElt, id) {
	mylog.log("loadCalendar",typeElt, id);

	$.ajax({
		type: "POST",
		url: baseUrl+'/co2/element/getdatadetail/type/'+typeElt+
					'/id/'+id+'/dataName/events',
		data: {},
		dataType: "json"
	}).done(function(data){
		mylog.log("loadCalendar", data);
		$("#profil-content-calendar").fullCalendar('destroy');
		calendar.showCalendar("#profil-content-calendar", data, "month");
		$("#profil-content-calendar").fullCalendar("gotoDate", moment(Date.now()));
		$(window).on('resize', function(){
			$("#profil-content-calendar").fullCalendar('destroy');
			calendar.showCalendar("#profil-content-calendar", data, "month");
		});
	}).fail(function(error) {
		$('.ret').html(error);
	});

	
}

function isContributor() {
	mylog.log("isContributor", userId,  elt);
	if( typeof userId != "undefined" &&
		userId != null &&
		userId != "" &&
		typeof elt != "undefined" && 
		typeof elt.links != "undefined" && 
		typeof elt.links.contributors != "undefined" && 
		typeof elt.links.contributors[userId] != "undefined" ) {
		$('#follows').html("Désinscrire");
		$('#follows').data("isco", true);
	} else {
		$('#follows').html("S'inscrire");
		$('#follows').data("isco", false);
	}
}

function initDescs() {
	mylog.log("inintDescs");
	var descHtml = "<i>"+trad.notSpecified+"</i>";
	if($("#descriptionMarkdown").html().length > 0){
		descHtml = dataHelper.markdownToHtml($("#descriptionMarkdown").html()) ;
	}
	$("#descriptionAbout").html(descHtml);
	mylog.log("descHtml", descHtml);
}

function toggleDescription(){
	$('#projectDescription button.customBtnTrigger').click(function(){
		if($(this).hasClass('open')){
			$('#projectDescription #descriptionAbout').css({'max-height': '148px', 'overflow': 'hidden'});
			$(this).removeClass('open').html('Lire la suite <i class="fa fa-angle-down"></i>');
		}
		else{
			$('#projectDescription #descriptionAbout').css({'max-height': 'none', 'overflow': 'auto'});
			$(this).addClass('open').html('Réduire <i class="fa fa-angle-up"></i>');
		}
	});
}
function toggleAllProjects(){
	$('.toggleAllProjects').click(function(){
		if( $(this).hasClass('open') ) {
			$('.projectNavThirdLvl').hide();
			$(this).removeClass('open');
			$('.toggleProjects').removeClass('fa-minus').addClass('fa-plus');
		}
		else{
			$('.projectNavThirdLvl').show();
			$(this).addClass('open');
			$('.toggleProjects').removeClass('fa-plus').addClass('fa-minus');
		}
	});
}
function toggleProjects(){
	$('.toggleProjects').click(function(){
		if( $(this).hasClass('fa-plus') ){
			$(this).removeClass('fa-plus').addClass('fa-minus');
		}
		else{
			$(this).removeClass('fa-minus').addClass('fa-plus');
		}

		$(this).parents('li').next('.projectNavThirdLvl').toggle();
	});
}
function toggleNavMobile(){
	$('.projectNavTriggerMobile').click(function(e){
		e.preventDefault();
		if(!$('.projectNav').hasClass('isAnimated')){

			$('.projectNav').addClass('isAnimated');
			if($(this).hasClass('active')){
				projectCloseMobileNav();
			}
			else{
				projectOpenMobileNav();
			}
		}
	});
}
function projectCloseMobileNav(){
	$('.projectNav').animate({left: '-100%'}, 200, function(){
		$('.projectNavTriggerMobile').removeClass('active');
		$('.projectNav').removeClass('isAnimated');
	});
}
function projectOpenMobileNav(){
	$('.projectNav').animate({left: '0'}, 300, function(){
		$('.projectNavTriggerMobile').addClass('active');
		$('.projectNav').removeClass('isAnimated');
	});
}
function showProjectMask(){
	$('.projectMask').show();
}
function hideProjectMask(){
	$('.projectMask').hide();
}
function customTabs(){
	$('.customTab').hide();
	$('.customTabTrigger').click(function(e){
		e.preventDefault();

		var $this = $(this);
		var href = $this.attr('href');
		var $tab = $(href);

		if( $tab.hasClass('active') ){
			$tab.hide().removeClass('active');
			$this.removeClass('active');
		}
		else{
			$('.customTab').hide().removeClass('active');
			$('.customTabTrigger').removeClass('active');
			$tab.show().addClass('active');
			$this.addClass('active');
		}
	});
	$('.closeCustomTab').click(function(e){
		e.preventDefault();
		var $tab = $(this).parents('.customTab');
		$tab.hide().removeClass('active');
	});
}
function selectProjectStart(){
	var hash = window.location.hash;
	if(hash){
		$(".projectNav .linkMenu[data-slug='" + hash.substring(1) + "']").click();
	}
	else{
		$('.projectNav .projectNavFirstLvl .linkOrganization').click();
	}
}
function selectProjectChildren(){
	$("#projectChildren").on('click','.linkMenu',function(){
		var key  = $(this).data("key");
		$(".projectNav .linkMenu[data-key='" + key + "']").click();
	});
}

function selectProject(){
	$(".projectNav").on('click','.linkMenu',function(e){
		//e.preventDefault();
		$this = $(this);
		var key  = $this.data("key");
		var col  = $this.data("col");
		var color = $this.data("color");
		var countMembers = $this.data('countmembers');
		var countEvents = $this.data('countevents');
		var countProjects = $this.data('countprojects');
		var toggleBtn = $this.prev('i');
		if(toggleBtn.hasClass('fa-plus'))
			toggleBtn.click();

		$(".linkMenu").removeClass('active');
		$this.addClass('active');

		var colorHover = color.replace("1)",".8)" );
				colorHover = colorHover.replace(".82)",".6)");

		$('#tempStyle').remove();

		$('<style id="tempStyle">.projectBorderColorHover:hover{border-color:'+colorHover+' !important;}.projectBgColorHover:not(.active):hover{background-color:'+colorHover+' !important;}.projectBgColorActive.active{background-color:'+color+';}.projectNav .linkMenu.active::after{background-color:'+color+'}</style>').appendTo('head');

		$('.projectColorBorder').css('border-color', color);
		$('.projectBgColor').css('background-color', color);

		$('.customTab').hide().removeClass('active');
		$('.customTabTrigger').removeClass('active');

	 	$.ajax({
			type: "POST",
			url: baseUrl+'/co2/element/get/id/'+key+'/type/'+col,
			data: {},
			dataType: "json",
			beforeSend: showProjectMask()
		}).done(function(data){
			feedProject(data, countMembers, countEvents, countProjects);
    	//mylog.log( "Data Ajax : " + data.toSource() );
			//loadMyStream(col, key);
		}).fail(function(error) {
    	//mylog.log( "error : " + error.toSource() );
    	$('.ret').html(error);
	  }).always(function(){
	  	hideProjectMask();
	  	if(isMobile()) 
	  		projectCloseMobileNav();
	  });
	});
}
function feedProject(el, countMembers, countEvents, countProjects){

 	//mylog.log('AJAX return : ' + el.toSource());
	
	// name
	elt = el.map;
	var id = el.map._id.$id ;
	$(".projectName, .projectNameParent").html(el.map.name);

	$("#follows").data( "id", id);

	// shortdecription
 	var shortDescription = el.map.shortDescription ? el.map.shortDescription : "";
 	if(el.map.shortDescription){
 		$(".projectShortDescription").show().html(el.map.shortDescription);
 	}
 	else{
 		$(".projectShortDescription").hide();
 	}

 	// description
 	if(el.map.description){
 		$(".customTabTriggerDescr").show();
 		if(el.map.description.length > 0){
			descHtml = dataHelper.markdownToHtml(el.map.description) ;
		}
 		$("#descriptionAbout").html(descHtml);
 	}
 	else{
 		$(".customTabTriggerDescr").hide();
 	}

 	// banner image
 	var bannerImgUrl = el.map.profilBannerUrl ? baseUrl + el.map.profilBannerUrl : defaultBannerUrl;
 	$(".projectBanner").css('background-image', 'url(' + bannerImgUrl + ')');
 	//alert(getBaseUrl() + el.map.profilBannerUrl);

 	// thumb image
 	var thumbImgUrl = el.map.profilMediumImageUrl ? baseUrl + el.map.profilMediumImageUrl : defaultBannerUrl;
 	$(".projectThumb").css('background-image', 'url(' + thumbImgUrl + ')');

 	// Members count

 	$('.projectNbMembers').text(countMembers);
 	$('.projectNbProjects').text(countProjects);
 	$('.projectNbEvents').text(countEvents);

 	// Project List
 	
 	$('#projectChildren .row').html('');

 	if( el.map.links.projects != null ){

 		$('.customTabTriggerProj').show();

	  $(el.map.links.projects).each(function(){
	 		//mylog.log("Project : " + this.toSource() );
	 		for (key in this) {

			  mylog.log("ProjectKey : " + key );

			  var $link = $('.linkMenu[data-key="' + key + '"]');
	 			var projectName = $link.text();
	 			var projectCollection = $link.attr('data-col');
	 			var projectImage = $link.attr('data-img');
	 			var htmlProjects = "<div class='col col-xs-6 col-md-3'>" +
	 			"<a href='#" + el.map.slug + "' class='linkMenu' data-col='" + projectCollection + "'" + 
	 			"data-key='" + key + "' style='background-image: url(" + projectImage + ");'>" + 
	 			"<span class='projectBorderColorHover'><h4>" + projectName+ "</h4></span></a></div>";

	 			$(htmlProjects).appendTo('#projectChildren .row');

			}
	 	});
 	}
 	else{
 		$('.customTabTriggerProj').hide();
 	}

 	// CONTACTS

 	$('#projectContacts .row').html('');

 	if( el.map.contacts != null ){
 		$('.customTabTriggerContacts').show();

 		 $(el.map.contacts).each(function(index){
	 		//mylog.log("Project : " + this.toSource() );
	 			var htmlContacts = "<div class='col col-xs-6 col-md-3'>" +
	 			"<h4>" + el.map.contacts[index]['name'] + "</h4>" +
	 			"<a href='mailto:"+el.map.contacts[index]['email']+"'>"+el.map.contacts[index]['email']+"</a>" +
	 			"<br><a href='phone:"+el.map.contacts[index]['telephone'][0]+"'>"+el.map.contacts[index]['telephone'][0]+"</a>" +
	 			"</div>";
	 			$(htmlContacts).appendTo('#projectContacts .row');
	 
	 	});
 	} else{
 		$('.customTabTriggerContacts').hide();
 	}
 	isContributor();
 	loadMyStream('projects', id);
 	loadCalendar('projects', id);

}

function loadMyStream(typeElt, id){
	mylog.log("loadMyStream", typeElt, id);
	$('#journal').html("");
    var url = "news/co/index/type/"+typeElt+"/id/"+id;///date/"+dateLimit;
    setTimeout(function(){ //attend que le scroll retourn en haut (kscrollto)
        showLoader('#journal');
        ajaxPost('#journal', baseUrl+'/'+url, 
            {
                nbCol: 1,
                inline : true
            },
            function(){ 
               // loadLiveNow();
            },"html");
    }, 700);
}
function changeMobileBtnMenu(){
	$('.btn-show-mainmenu i').removeClass('fa-bars').addClass('fa-cog'); 
}
function isMobile(){
	return ( $(window).outerWidth() < 997 ) ? true : false ;
}